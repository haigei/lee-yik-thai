#include <iostream>
#include<limits>
#include<conio.h>
#include<math.h>
using namespace std;
void function1();
void function2();
void function3();
void function4();
void function5();
void function6();
void function7();
void function8();

int main()
{
    int option=0;
	do
	{
                cout << "Menu Driven Program" << endl;
                cout << "--------------------------------------" << endl;
                cout << "1. Convert Celsius to Fahrenheit" << endl;
                cout << "2. Convert Fahrenheit to Celsius" << endl;/*sdsfsffsdf*/
                cout << "3. Calculate Circumference of a circle" << endl;
                cout << "4. Calculate Area of a circle" << endl;
                cout << "5. Area of Rectangle" << endl;
                cout << "6. Area of Triangle (Heron��s Formula)" << endl;
                cout << "7. Volume of Cylinder" << endl;
                cout << "8. Volume of Cone" << endl;
                cout << "9. Quit program" << endl;
                cout << "Please enter your option: ";
                cin>>option;
                 while ( cin.fail() || option < 1 || option > 9)
                {
                        cin.clear();
                        cin.ignore();
                        cout<<"Invaild input, please insert again"<<endl;
                        cin>>option;
                }

        switch(option)
        {
        	case 1:{
        			cin.clear();
                    cin.ignore();
        			function1();
        			break;
        	}
        	case 2:{
        			cin.clear();
                    cin.ignore();
        			function2();
        			break;
        	}
            case 3:{
            		cin.clear();
                    cin.ignore();
                    function3();
                    break;
            }
            case 4:{
            		cin.clear();
                    cin.ignore();
                    function4();
                    break;
            }
            case 5:{
            		cin.clear();
                    cin.ignore();
                    function5();
                    break;
            }
            case 6:{
            		cin.clear();
                    cin.ignore();
                    function6();
                    break;
            }
            case 7:{
            		cin.clear();
                    cin.ignore();
                    function7();
                    break;
            }
            case 8:{
            		cin.clear();
                    cin.ignore();
                    function8();
                    break;
            }
        	case 9:{
        			cin.clear();
                    cin.ignore();
        			cout<<"Bye Bye"<<endl;
        			return 0;
        	}
        	default:
        		cout<<"Wrong input"<<endl;
        }
	}while(option!=9);
	return 0;
}

void function1()
{
        float fah,cel;

        cout << "Convert Celsius to Fahrenheit" << endl;
        cout << "Please enter Celsius : ";
        cin>>cel;
        while ( cin.fail())
        {
            cin.clear();
            cin.ignore();
            cout << "Invaild input, please insert again" << endl;
            cin >> cel;
        }
        fah = (cel*1.8)+32;
        cout << "Fahrenheit : " << fah << endl;
        cout << "--------------------------------------" <<endl;
}

void function2()
{
        float fah,cel;

        cout << "Convert Fahrenheit to Celsius" << endl;
        cout << "Please enter Fahrenheit : ";
        cin>>fah;
        while ( cin.fail())
        {
            cin.clear();
            cin.ignore();
            cout << "Invaild input, please insert again" << endl;
            cin >> fah;
        }
        cel = (fah-32)/1.8;
        cout << "celsius : " << cel << endl;
        cout << "--------------------------------------" <<endl;
}

void function3()
{
	const double p1 = 3.14;
	double radius;
	double diameter;
	double circumference;
	cout << "Calculate Circumference of a circle" << endl;
	cout << "Please enter radius of circle: ";
	cin >> radius;
	while ((1 > radius) || cin.fail())
	{
		cin.clear();
		cin.ignore();
		cout << "Invaild input, please insert again" << endl;
		cin >> radius;
	}
	diameter = 2 * radius;
	circumference = p1 * diameter;
	cout << "Circumference of a circle: " << circumference << endl;
	cout << "--------------------------------------" << endl;
	cout << endl;

}
void function4()
{
	const double p1 = 3.14;
	double area;
	double radius;
	cout << "Calculate Area of a circle" << endl;
	cout << "Please enter radius of circle: ";
	cin >> radius;
	while ((1 > radius) || cin.fail())
	{
		cin.clear();
		cin.ignore();
		cout << "Invaild input, please insert again" << endl;
		cin >> radius;
	}
	area = p1 * (radius*radius);
	cout << "Area of circle: " << area << endl;
	cout << "--------------------------------------" << endl;
	cout << endl;

}
void function5()
{
    int area, l, h;
    cout << "Area of Rectangle" << endl;
    cout << "Please enter the length of Rectangle: ";
    cin >> l;
    while ( (1 > l) || cin.fail() )
    {
            cin.clear();
            cin.ignore();
            cout<<"Invaild input, please insert again"<<endl;
            cin>>l;
    }
    cout << "Please enter the high of Rectangle: ";
    cin >> h;
    while ( (1 > h) || cin.fail() )
    {
            cin.clear();
            cin.ignore();
            cout<<"Invaild input, please insert again"<<endl;
            cin>>h;
    }
    area = l*h;
    cout << "Area of Rectangle: " << area << endl;
    cout << "--------------------------------------" << endl;
    cout << endl;
}
void function6()
{
        float a, b, c, area, s;
        cout << "Area of Triangle" << endl;
        cout << "Please enter the first side of Triangle: ";
        cin >> a;
        while ( (1 > a) || cin.fail() )
        {
                cin.clear();
                cin.ignore();
                cout<<"Invaild input, please insert again"<<endl;
                cin>>a;
        }
        cout << "Please enter the second side of Triangle: ";
        cin >> b;
        while ( (1 > b) || cin.fail() )
        {
                cin.clear();
                cin.ignore();
                cout<<"Invaild input, please insert again"<<endl;
                cin>>b;
        }
        cout << "Please enter the third side of Triangle: ";
        cin >> c;
        while ( (1 > c) || cin.fail() )
        {
                cin.clear();
                cin.ignore();
                cout<<"Invaild input, please insert again"<<endl;
                cin>>c;
        }
        s=(a+b+c)/2;
        area = sqrt(s*(s-a)*(s-b)*(s-c));
        cout << "Area of Triangle: " << area << endl;
        cout << "--------------------------------------" << endl;
        cout << endl;
}
void function7()
{
        float p1=3.14,r,h,vol;
        cout << "Volume of Cylinder" << endl;
        cout << "Please enter the Height of Cylinder: ";
        cin >> h;
        while ( (1 > h) || cin.fail() )
        {       
                cin.clear();
                cin.ignore(); 
                cout<<"Invaild input, please insert again"<<endl;
                cin>>h;           
        }
        cout << "Please enter the radius of Cylinder: ";
        cin >> r;
        while ( (1 > r) || cin.fail() )
        {       
                cin.clear();
                cin.ignore(); 
                cout<<"Invaild input, please insert again"<<endl;
                cin>>r;           
        }
        vol=(p1*r*r*h);
        cout << "Volume of Cylinder: " << vol << endl;
        cout << "--------------------------------------" << endl;
        cout << endl;
}

void function8()
{
        float p1=3.14,r,h,vol;
        cout << "Volume of Cone" << endl;
        cout << "Please enter the Height of Cone: ";
        cin >> h;
        while ( (1 > h) || cin.fail() )
        {       
                cin.clear();
                cin.ignore(); 
                cout<<"Invaild input, please insert again"<<endl;
                cin>>h;           
        }
        cout << "Please enter the radius of Cone: ";
        cin >> r;
        while ( (1 > r) || cin.fail() )
        {       
                cin.clear();
                cin.ignore(); 
                cout<<"Invaild input, please insert again"<<endl;
                cin>>r;           
        }
        vol=(p1*r*r*h/3);
        cout << "Volume of Cone: " << vol << endl;
        cout << "--------------------------------------" << endl;
        cout << endl;
}
